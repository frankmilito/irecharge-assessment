const Customer = require("../models/customerModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { promisify } = require("util");

exports.protect = (req, res, next) => {};

exports.register = async (req, res, next) => {
  try {
    let customer = await Customer.findOne({ email: req.body.email });
    if (customer) {
      //   return next("This email is already registered");
      res.status(400).json({
        status: "fail",
        message: "This email is already registered please log in!",
      });
      return;
    }

    customer = await Customer.create({
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      role: req.body.role,
      password: req.body.password,
      passwordConfirm: req.body.passwordConfirm,
    });
    const token = jwt.sign(
      { id: customer._id, role: req.body.role },
      process.env.JWT_SECRET
    );

    res.status(201).json({
      status: "success",
      message: "Customer successfully registered",
      token,
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err.message,
    });
  }
};

exports.login = async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({
      status: "fail",
      message: "Please provide email and password",
    });
    return;
  }
  try {
    const customer = await Customer.findOne({ email }).select("+password");
    if (!customer) {
      res.status(400).json({
        status: "fail",
        message: "Invalid email or password",
      });
      return;
    }
    const isPasswordValid = await bcrypt.compare(password, customer.password);

    if (!isPasswordValid) {
      res.status(400).json({
        status: "fail",
        message: "Invalid email or password",
      });
      return;
    }
    const token = jwt.sign({ id: customer._id }, process.env.JWT_SECRET);
    customer.password = undefined;
    res.status(201).json({
      status: "success",
      data: {
        customer,
        token,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err.message,
    });
  }
};

exports.protect = async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) {
    res.status(401).json({
      status: "Unauthorized access",
    });
    return;
  }

  //verify the token
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  //check if the user still exists/ has not been deleted
  const existingUser = await Customer.findById(decoded.id);
  if (!existingUser) {
    res.status(400).json({
      status: "fail",
      message: "Customer not found",
    });
    return;
  }

  req.user = existingUser;
  next();
};

exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return res.status(403).json({
        status: "fail",
        message: "You do not have permission to perform this action",
      });
    }
    next();
  };
};

exports.updatePassword = async (req, res, next) => {
  const customer = await Customer.findById(req.user._id).select("+password");
  console.log(customer);
};
