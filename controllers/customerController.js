const { response } = require("express");
const Customer = require("../models/customerModel");
const Payment = require("../models/paymentModel");

exports.getAllCustomers = async (_, res) => {
  try {
    const customers = await Customer.find();
    res.status(200).json({
      status: "success",
      records: customers.length,
      data: {
        customers,
      },
    });
  } catch (err) {
    res.status(500).json({
      status: "fail",
      message: err.message,
    });
  }
};

exports.getPaymentsForCustomer = async (req, res, next) => {
  try {
    const payments = await Payment.find({ customer: req.user._id });
    res.status(200).json({
      status: "success",
      data: {
        payments: payments,
      },
    });
  } catch (err) {
    res.status(500).json({
      status: "fail",
      message: err.message,
    });
  }
};

exports.getCustomer = async (req, res, next) => {
  try {
    if (!req.params.id) {
      return res.status(400).json({
        status: "fail",
        message: "Invalid customer ID",
      });
    }
    const customer = await Customer.findById(req.params.id)
      .select("-__v")
      .populate({
        path: "payments",
        select: "amount -customer status flutterRef",
      });

    if (!customer) {
      return res.status(404).json({
        status: "fail",
        message: "No customer with that ID was found",
      });
    }
    res.status(200).json({
      status: "success",
      data: {
        customer: customer,
      },
    });
  } catch (err) {
    res.status(500).json({
      status: "fail",
      message: err.message,
    });
  }
};

exports.deleteCustomer = async (req, res) => {
  const { id } = req.params;

  try {
    const customer = await Customer.findByIdAndDelete(id);
    if (!customer) {
      return res
        .status(400)
        .json({ status: "fail", message: "No customer found with that ID" });
    }

    res.status(204).json({ status: "success", message: "Ok" });
  } catch (err) {
    res.status(500).json({
      status: "fail",
      message: err.message,
    });
  }
};

exports.updateMe = async (req, res) => {
  const { _id } = req.user;
  if (req.body.password || req.body.passwordConfirm) {
    return res.status(400).json({
      status: "fail",
      message:
        "This route is not for password updates. Please use /update-password.",
    });
  }
  try {
    await Customer.findByIdAndUpdate(_id, req.body, {
      new: true,
      runValidators: true,
    });

    res.status(200).json({ status: "success" });
  } catch (err) {
    res.status(500).json({
      status: "fail",
      message: err.message,
    });
  }
};
