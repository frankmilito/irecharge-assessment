const Flutterwave = require("flutterwave-node-v3");
const Payment = require("../models/paymentModel");
// const Customer = require("../models/customerModel");
const flw = () =>
  new Flutterwave(process.env.FLW_PUBLIC_KEY, process.env.FLW_SECRET_KEY);

exports.payment = async (req, res) => {
  const { amount, tx_ref, card_number, cvv, enckey } = req.body;

  try {
    if (!amount || !card_number || !cvv || !enckey || !tx_ref) {
      res.status(400).json({
        status: "fail",
        message: "Please provide all details",
      });
      return;
    }

    const response = await flw().Charge.card(req.body);
    await Payment.create({
      amount,
      transactionRef: tx_ref,
      status: response.data.status,
      customer: req.user._id,
      flutterRef: response.data.flw_ref,
    });

    res.status(200).json(response);
  } catch (err) {
    res.status(500).json({ status: "fail", message: err.message });
  }
};

exports.validate = async (req, res) => {
  try {
    const id = req.params.id;

    const response = await flw().Charge.validate({
      otp: req.body.otp,
      flw_ref: req.body.flw_ref,
    });

    await Payment.findByIdAndUpdate(
      id,
      {
        status: response.data.status,
      },
      { new: true, runValidators: true }
    );

    res.status(200).json({
      status: "success",
      response,
    });
  } catch (err) {
    res.status(500).json({ status: "fail", message: err.message });
  }
};

exports.getAllPayments = async (req, res, next) => {
  console.log(req.query.status);
  // if (req.query.status !== "successful" || req.query.status !== "pending") {
  //   return res.status(400).json({
  //     status: "fail",
  //     message: "status is invalid",
  //   });
  // }
  try {
    const payments = await Payment.find(req.query);

    res.status(200).json({
      status: "success",
      payments: payments,
    });
  } catch (err) {
    res.status(500).json({ status: "fail", message: err.message });
  }
};
