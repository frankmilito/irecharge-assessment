const mongoose = require("mongoose");

const paymentSchema = new mongoose.Schema(
  {
    amount: {
      type: Number,
      required: [true, "Amount can't be empty"],
    },
    transactionRef: {
      type: String,
      required: [true, "Transaction reference is required"],
    },
    flutterRef: String,
    status: {
      type: String,
      default: "pending",
      enum: ["pending", "successful"],
    },
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    customer: {
      type: mongoose.Types.ObjectId,
      ref: "Customer",
      required: [true, "Payment must belong to a customer"],
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

const Payment = mongoose.model("Payment", paymentSchema);

paymentSchema.pre(/^find/, function (next) {
  this.populate({
    path: "customer",
    select: "name email",
  });
  next();
});
module.exports = Payment;
