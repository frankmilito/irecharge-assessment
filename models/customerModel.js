const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const customerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "A customer name is required"],
    },
    phone: {
      type: String,
      required: [true, "A customer phone number is required"],
    },
    email: {
      type: String,
      required: [true, "A customer email is required"],
    },
    role: {
      type: String,
      default: "customer",
      enum: ["customer", "admin"],
    },
    password: {
      type: String,
      minLength: 4,
      required: [true, "Password is required"],
      select: false,
    },
    passwordConfirm: {
      type: String,
      required: [true, "Please confirm your password"],
      validate: {
        //Only works for create and save
        validator: function (val) {
          return val === this.password;
        },
        message: "Passwords do not match",
      },
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

customerSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;
  next();
});

customerSchema.virtual("payments", {
  ref: "Payment",
  foreignField: "customer",
  localField: "_id",
});

const Customer = mongoose.model("Customer", customerSchema);

module.exports = Customer;
