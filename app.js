const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const customerRoutes = require("./routes/customerRoutes");
const payment = require("./routes/payment");

const app = express();
app.use(helmet());
app.use(express.json());

if (process.env.NODE_ENV === "development") app.use(morgan("dev"));
app.use("/api/v1/payments", payment);
app.use("/api/v1/customers", customerRoutes);

module.exports = app;
