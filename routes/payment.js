const express = require("express");
const paymentController = require("../controllers/paymentController");
const authController = require("../controllers/authController");

const router = express.Router();
router.use(authController.protect);
router.get("/initialize", paymentController.payment);
router.get("/", paymentController.getAllPayments);
router.get("/validate/:id", paymentController.validate);

module.exports = router;
