const express = require("express");
const authController = require("../controllers/authController");
const customerController = require("../controllers/customerController");

const router = express.Router();

router.post("/register", authController.register);
router.post("/login", authController.login);

router.use(authController.protect);
router.get("/", customerController.getAllCustomers);
router.get("/me", customerController.getPaymentsForCustomer);
router.get("/:id", customerController.getCustomer);
router.delete(
  "/:id",
  authController.restrictTo("admin"),
  customerController.deleteCustomer
);
router.patch("/update", customerController.updateMe);
router.patch("/update-password", authController.updatePassword);

module.exports = router;
